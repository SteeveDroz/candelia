# Journal de Pekarion fils d'Eirik

*Duncastle, le 20<sup>ème</sup> jour de la 1<sup>ère</sup> lune d'été de l'an 996*

Je suis candélien.
Qu'est-ce que ça veut dire ?
Je suis un homme, je fais partie de ce pays, je vis parmi ce peuple et j'intéragis avec mon entourage.
Mais au fond, tout le monde fait ça, non ?
Qu'est-ce que ça veut dire d'être candélien ?
Depuis bientôt mille ans, des gens se sont succédés et ont marché dans les bois de ce pays, dans les rues de ces cités, dans les couloirs de ce château comme dans les bas-fonds de la plus vile des tavernes.
De nobles rois, de puissants guerriers, des marchants qui ont gagné leur noblesse à la force de leur réputation ; tous ont, à leur manière, tenu la place qui leur était octroyée dans ce monde.
Et moi, que fais-je dans ce monde ?

Je ne suis pas roi.
Tout le monde affirme que si, certains s'en glorifient, tous – ou en tout cas beaucoup – pensent que je suis un bon roi, mais je sais bien que ce n'est pas vrai.

Ma mère me l'a dit, elle aurait sans aucun doute voulu taire ce terrible et lourd secret, mais *la fiole* étant nécessaire à cacher les apparences, il lui était impossible de me garder dans l'ignorance.
Mère est morte hier, je suis à présent le seul dépositaire de ce lourd fardeau.
Peut-être vais-je en toucher un mot à [Zelina](personnes/zelina.md)… peut-être.
L'an passé, l'[incident de la boulangère](anecdotes/incident-de-la-boulangere.md) a presque révélé au monde ma vraie nature, fort heureusement, mère était là pour régler les choses.
Maintenant qu'elle s'en est allé, je vis dans la crainte perpétuelle d'un nouvel incident.

Je ne suis pas roi.
Mais alors, qui suis-je ? Un traitre ? Un imposteur ? Un usurpateur ?
J'ai été placé à un poste pour lequel je ne suis pas fait et, malgré ce qui est arrivé lors de la [chasse au cerf blanc](anecdotes/chasse-au-cerf-blanc.md), je ne suis ni assez digne, ni assez noble pour mériter ce titre.

Et mon père – c'est-à-dire le roi [Eirik](personnes/eirik.md), s'il avait connu ma véritable nature, m'aurait-il renié ?
Ma mère le lui a-t-elle seulement dit, même sur son lit de mort ?
Cet homme, cette figure paternelle, même s'il n'est pas mon géniteur, a toujours été pour moi ce qui ressemblait le plus à un modèle.
Je veux lui ressembler en tout point, je ne puis le décevoir.
Être le fils d'un autre est un déshonneur à ses yeux, même s'il ignore tout de ce fait ; je le devine dans ses yeux, comme s'il le savait au fond de lui.

Jusqu'à hier, j'étais le fils d'[Arianna](personnes/arianna.md) et ça me suffisait.
Par son sang et son alliance avec Eirik, j'étais le roi – ou du moins j'acceptais ce titre.
Mais à présent qu'elle n'est plus, il n'y a dans tout Candelia âme qui vive qui me relie de près ou de loin au trône, cette pièce de bois couverte d'or qui surplombe une salle gigantesque, dans le palais de Duncastle.
Une pièce de bois couverte d'or, c'est bien ce que je suis.
Un morceau de bois moisi, grossièrement taillé dans une branche d'arbre entée qu'on a poli et couvert d'une fine couche de métal précieux afin de faire croire au monde qu'elle a une quelconque valeur.
Ma place n'est pas ici, je vais abdiquer.
Je partirai loin de ce pays, je m'enfoncerai dans Aberskye pour ne jamais en ressortir, ou je prendrai un bateau pour me perdre dans des contrées où nul candélien n'a encore osé poser le pied.
Cela ferait de moi un homme à part, un homme bon qui connait ses limites et qui rend au monde ce qui ne lui appartient pas.

Abdiquer.

C'est peut-être finalement plus simple comme cela.
Plus simple pour moi, oui.
Et mon peuple ?
Que va-t-il penser d'un roi qui baisse les bras ?
Vont-ils scander mon nom dans les rues, célébrant mon audace et mon courage, comme ils l'ont fait après le [Conseil de Dorkando](anecdotes/conseil-de-dorkando.md) ?
Ou au contraire vont-ils jeté l'opprobre sur moi ?
Oui, c'est plus plausible.
Le peuple ne conprendrait pas ; pour eux, je suis et je resterai le Roi Perakion ap Eirik, noble par sa naissance et par ses actions.
Ce statut est ancré dans leur cœur, ils me considèrent comme un roi, quoi qu'il advienne.
Je pourrais être un chien, que même les barbares ne s'abaisseraient pas à nourrir, ils me révèreraient et me suivraient encore.
Quels fous !

Je suis le roi des fous.

Ils suivraient un batard qu'une femme trop aimante a mis au monde pour sauver l'honneur d'un vieux roi impuissant.
S'ils savaient, ils l'adoreraient pour ce geste, elle a à elle seule sauvé la lignée royale et la noblesse de mon père.

Je ne vais rien dire, rien.
Leur avouer ce secret à présent, c'est détruire le sacrifice que mère a fait, c'est trahir sa confiance et c'est déshonorer père.
Qui suis-je pour entacher le souvenir d'un roi et de sa reine ?
Qui suis-je ?
Je suis celui qu'on suit, celui qu'on considère, celui qu'on croit et en qui on place sa confiance.
J'ai un devoir vers mon peuple, un devoir moral et séculaire.
Un devoir éternel.
Je ferai mon possible pour être un meilleur roi, un roi qui compte.
Je trouverai une solution à ce problème de descendance.
Je ne suis pas un simple candélien.

Je suis Candelia !

---

*Duncastle, le 3<sup>ème</sup> jour de la 2<sup>ème</sup> lune d'automne de l'an 997*

[Zelina](personnes/zelina.md) est vraiment une amie sincère.
Je peux tout lui dire et, même si j'essayais de lui cacher quoi que ce soit, elle saurait que je garde des secrets envers elle.
Oh, bien sûr jamais elle ne me mettrait au pied du mur, jamais elle ne chercherait à connaitre ce que je lui tais, elle respecte trop ma royauté pour ça.
Je l'aime de tout mon cœur, elle est ma sœur de lait, la fille de ma bien aimée nourrice.

Ce matin, nous avons marché dans le domaine du château, nous avons parlé de choses et d'autres, c'était agréable.
Et au moment où j'étais certain que personne ne nous entendait, je lui ai demandé d'un air grave d'écouter sans répondre à ce que j'avais à lui dire.
Elle est noble et respectueuse et pas une fois a-t-elle osé interrompre mon aveu ; aurait-elle même cillé lorsque je lui racontai mon plus terrible secret : la descendance brisée, la batardise du souverain et la décision de taire cette affaire, que je ne l'aurais pas remarqué.
J'étais trop pris dans mon histoire pour étudier ses réactions, c'était trop intense.
Et sa réaction fut contenue comme jamais je n'ai vu cacher un sentiment.
J'ai pu me mettre l'espace d'un instant dans la peau des pauvres malheureux qu'elle berne jour à jour, tous ceux qu'elle trahi en les espionnant pour mon compte, tous ceux contre qui elle manigance, ceux-ci n'auront jamais le moindre doute que Zelina est la plus habile des manipulatrice est des intrigantes.

Une fois cette information en sa possession, elle m'a simplement demandé « Et que comptes-tu faire concernant ta descendance ? »
Une simple question à laquelle je n'avais pas réfléchi.
Il faudra qu'un jour je me marie et que je promette à mon peuple l'assurance d'un avenir radieux avec un enfant premier né, mais il n'aura pas le loisir d'user de la même ruse que moi, avec la fiole de mère ; je ne suis même pas sûr qu'elle contienne assez de sang pour tenir jusqu'à la fin de mes jours.

Je l'ai chargé de réfléchir à ces épineuses questions et, avant de se séparer et de rentrer au château individuellement, je l'ai remercié d'être ma confidente et l'ai enlacé.
J'ai senti à sa réaction qu'elle ne s'attendait pas à ce genre de démonstrations.
Il faut dire que nous ne nous touchons que dans de rares occasions et elles sont toujours spéciales.
Si ceci n'est pas spécial, je ne sais pas ce qui l'est.
