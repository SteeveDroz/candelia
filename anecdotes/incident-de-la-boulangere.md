# Incident de la boulangère

Le 7<sup>ème</sup> jour de la 3<sup>ème</sup> lune de printemps en l'an 994 – le roi [Eirik](../personnes/eirik.md) venait de décéder – le jeune roi Pekarion dut officier lors du mariage de Fremda, la boulangère du château, et de Gustario, un assistant de Bemor, voyageur et aventurier qui se rendit aux confins sud de Dorkando pour des raisons diplomatiques et scientifiques.
Leur rencontre s'était faite au château, lorsque Bemor faisait son rapport de voyage au roi Pekarion.
Gustario s'était vu offert une collation aux cuisines et c'est Fremda qui le servit.
Un simple regard, une gentille attention et l'amour naquit entre ces deux personnes.

Apprenant le mariage et les circonstances de la rencontre, le conseiller du roi Pekarion cru bon d'inciter celui-ci à célébrer le mariage, bien qu'il ne représente aucun intérêt pour la couronne.
Écoutant ce sage conseil – et pensant que le peuple trouve bon que le roi officie pour de simples gens – Pekarion organisa la célébration vers la Pierre Royale.

Seulement, au moment d'entrer dans le cercle, Pekarion se retrouva bloqué à la périphérie de celui-ci.
Craignant le pire, il mit la main à sa poche secrète pour la trouver vide.
Horreur ! Il avait changé de costume, afin de se présenter aux fillancés dans ses plus beaux effets, mais avait oublié de transférer la *fiole* dans son manteau d'apparat.
Un rapide signe de tête à [Zelina](../personnages/zelina.md), une articulation silencieuse du mot *mère* et l'espionne galoppa au château pour quérir la reine douairière.
L'attente fut longue et Pekarion prétexta un léger malaise et insista pour qu'il reprenne ses esprits avant de commencer, craignant qu'un mal ne le prenne durant la cérémonie et qu'il gâche ainsi le plus beau jour de la vie de Gustario et Fremda.

Plus le temps passait, plus les esprits s'agitaient et plus les commérages allèrent bon train.
Au moment où il allait passer aux aveux, Pekarion aperçut au loin un destrier qui portait sa mère.
Il s'adressa alors aux futurs mariés en ces mots :

– Mes braves sujets, il est de mon devoir de vous avertir que cette prétendue indisposition était une supercherie destinée à vous faire patienter.
En effet, je voulais vous faire une surprise royale en conviant à votre union la reine [Arianna](../personnes/arianna.md).

Les personnes présentes furent étonnées d'un tel honneur offert à de simples servants, mais personne n'osa contrarier le projet du roi.

Après une subtile accolade à son fils, lors de laquelle la reine Arianna glissa subreptissement la source de son pouvoir, l'invitée d'honneur embrassa Fremda, puis Gustario.
L'honneur – et le secret – étaient saufs, mais pour combien de temps ?
Une erreur de plus et c'en aurait été fait de la royauté.
